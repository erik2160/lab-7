package university.jala.lab7.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import university.jala.lab7.Role;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(unique = true)
    private String username;

    @Column
    private String password;

    @Column
    private Role role;
}
