package university.jala.lab7.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.lab7.entity.User;
import university.jala.lab7.service.UserService;

@RestController
@RequestMapping("/auth")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    public ResponseEntity<User> insertUser(@RequestBody User user) throws Exception {
      userService.createUser(user);
      return ResponseEntity.ok(user);
    }
}
