package university.jala.lab7.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import university.jala.lab7.entity.User;
import university.jala.lab7.repository.UserRepository;

@Service
public class UserService {
    private final UserRepository repository;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
        encoder = new BCryptPasswordEncoder();
    }

    public User createUser(User user) throws Exception{
        user.setPassword(encoder.encode(user.getPassword()));
        return repository.save(user);
    }

    public Iterable<User> findAll() {
        return repository.findAll();
    }
}