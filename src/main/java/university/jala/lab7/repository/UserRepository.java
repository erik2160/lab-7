package university.jala.lab7.repository;

import org.springframework.data.repository.CrudRepository;
import university.jala.lab7.entity.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    Optional<User> findByUsername(String username);
}
